url = 'http://172.30.49.232:8080/video';
cam = ipcam(url);
% preview(cam);

n=0;
tic
while (toc < 8)
    n = n+1;
    frame = snapshot(cam);
    thisFrameRed(:,:) = frame(:,:,1);
    redMeans(n) = mean(thisFrameRed,'all');
end
Fs = (n/toc)*60; %frame per minute

figure(1)
plot(redMeans);
title("Red Color Brightness");
xlabel('frame');
ylabel('red color mean');
%%
myFFT = fft(redMeans);
halfFFT = myFFT(1:n/2+1);

fRepPeriod = Fs/n;

startFrq = 50;
finishFrq = 220;

startIndex = startFrq/fRepPeriod +1;
finishIndex = finishFrq/fRepPeriod +1;

BPMValues = abs(halfFFT(startIndex:finishIndex));
f = fRepPeriod*(0:finishIndex-startIndex)+startFrq;

figure(2)
plot(f,BPMValues)
title('BPM plot')
xlabel('BPM')
ylabel('Magnitude')
%%
[maxBPMval, maxBPMindex] = max(BPMValues);
maxBPM = f(maxBPMindex);

figure(3)
plot(f,BPMValues)
title('BPM plot')
xlabel('BPM')
ylabel('Magnitude')
text(maxBPM, maxBPMval, sprintf("Your BPM is :%f",maxBPM));
%%
PPGfft(1:n) = 0;
PPGfft(startIndex:finishIndex) = myFFT(startIndex:finishIndex);
PPGfft(n-finishIndex+1:n-startIndex+1) = myFFT(n-finishIndex+1:n-startIndex+1);

PPG = real(ifft(PPGfft));

figure(4)
plot(PPG)
title('PPG Signal')
xlabel('frame')
ylabel('red color mean')